
new Vue({
	el:'#main',
	created:function(){
		this.getItems();
	},
	data:{
		url:'teachers/',
		list:[],
		id_types:[],
		search:'',
		s:{fullname:'',id_type:'',identification:'',birthdate:'',main_phone:'',sec_phone:'',email:'',observations:''},
		c:{name:'',lastname:'',id_type:'',identification:'',birthdate:'',main_phone:'',sec_phone:'',email:'',observations:''},
		e:{id:'',person:'',name:'',lastname:'',id_type:'',identification:'',birthdate:'',main_phone:'',sec_phone:'',email:'',observations:''},
		errors:[]
	},
	computed:{
		
	},
	methods:{
		searchItems:function(){
			//this.search='Buscando '+this.search
		},
		
		getItems:function(){
			var url=this.url;
			axios.get(url).then(response=>{
				this.list=response.data.teachers;
				this.id_types=response.data.id_types;
			});
		},
		

		delItem:function(item){
			/*var url=this.url + item.id;
			axios.delete(url).then(response=>{
				this.getQuestions();
				toastr.success('pregunta eliminada correctamente');
			});*/
		},
		showItem:function(item){
			var url=this.url+item.id;
			this.s={
				fullname:item.name+" "+item.lastname,
				id_type:item.id_type,
				identification:item.identification,
				birthdate:item.birthdate,
				main_phone:item.main_phone,
				sec_phone:item.sec_phone,
				email:item.email,
				observations:item.observations
			};
			$("#show").modal('show');
		},
		editItem:function(item){
			
			this.e={
				//id:item.id,
				name:item.name,
				lastname:item.lastname,
				id_type:item.id_type_key,
				identification:item.identification,
				birthdate:item.birthdate,
				email:item.email,
				main_phone:item.main_phone,
				sec_phone:item.sec_phone,
				person:item.person,
				observations:item.observations
			};
			$("#edit").modal('show');

		},
		updateItem:function(id){
			var url=this.url+id;
			//alert(url);
			axios.put(url,this.e).then(response=>{
				this.getItems();
				this.errors=[];
				$('#edit').modal('hide');
				toastr.success('Datos actualizados correctamente');
			}).catch(error=>{
				this.errors=error.response.data
			});
			//$("#show").modal('show');
		},
		createItem:function(){
			var url=this.url;
			//alert(url);

			axios.post(url,{
				name:this.c.name,
				lastname:this.c.lastname,
				id_type:this.c.id_type,
				identification:this.c.identification,
				main_phone:this.c.main_phone,
				sec_phone:this.c.sec_phone,
				email:this.c.email,
				})
			.then(response=>{
				this.getItems();
				this.errors=[];
				$('#create').modal('hide');
				toastr.success('Docente registrado con éxito!')
			}).catch(error=>{
				this.errors=error.response.data
			});
		}
	}
})