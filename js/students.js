
new Vue({
	el:'#main',
	created:function(){
		this.getItems();
	},
	data:{
		url:'students/',
		list:[],
		id_types:[],
		course_categories:[],
		search:'',
		s:{fullname:'',id_type:'',identification:'',birthdate:'',main_phone:'',sec_phone:'',email:'',interest_courses:'',observations:''},
		c:{name:'',lastname:'',id_type:'',identification:'',birthdate:'',main_phone:'',sec_phone:'',email:'',interest_courses:[],observations:''},
		e:{id:'',person:'',name:'',lastname:'',id_type:'',identification:'',birthdate:'',main_phone:'',sec_phone:'',email:'',observations:''},
		errors:[]
	},
	computed:{
		
	},
	methods:{
		searchItems:function(){
			//this.search='Buscando '+this.search
		},
		enrolment:function(item){
			//alert("Matricula en progreso!");
			var url= this.url+'enrolment/'+item.id;
			//alert(url);
			axios.put(url,this.e.id).then(response=>{
				this.getItems;
				this.errors=[];
				$('#edit').modal('hide');
				toastr.success('Se hamatriculado correctamente');
			}).catch(error=>{
				this.errors=error.response.data
			});
		},
		getItems:function(){
			var url=this.url;
			axios.get(url).then(response=>{
				this.list=response.data.students;
				this.id_types=response.data.id_types;
				this.course_categories=response.data.course_categories;
			});
		},
		

		delItem:function(item){
			/*var url=this.url + item.id;
			axios.delete(url).then(response=>{
				this.getQuestions();
				toastr.success('pregunta eliminada correctamente');
			});*/
		},
		showItem:function(item){
			var url=this.url+item.id;
			this.s={
				fullname:item.name+" "+item.lastname,
				id_type:item.id_type,
				identification:item.identification,
				birthdate:item.birthdate,
				main_phone:item.main_phone,
				sec_phone:item.sec_phone,
				email:item.email,
				observations:item.observations
			};
			$("#show").modal('show');
		},
		editItem:function(item){
			
			this.e={
				//id:item.id,
				name:item.name,
				lastname:item.lastname,
				id_type:item.id_type_key,
				identification:item.identification,
				birthdate:item.birthdate,
				email:item.email,
				main_phone:item.main_phone,
				sec_phone:item.sec_phone,
				person:item.person,
				observations:item.observations
			};
			$("#edit").modal('show');

		},
		updateItem:function(id){
			var url=this.url+id;
			//alert(url);
			axios.put(url,this.e).then(response=>{
				this.getItems();
				this.errors=[];
				$('#edit').modal('hide');
				toastr.success('Datos actualizados correctamente');
			}).catch(error=>{
				this.errors=error.response.data
			});
			//$("#show").modal('show');
		},
		createItem:function(){
			var url=this.url;
			//alert(url);

			axios.post(url,{
				name:this.c.name,
				lastname:this.c.lastname,
				id_type:this.c.id_type,
				identification:this.c.identification,
				main_phone:this.c.main_phone,
				sec_phone:this.c.sec_phone,
				email:this.c.email,
				interest_courses:this.c.interest_courses
				})
			.then(response=>{
				this.getItems();
				this.errors=[];
				$('#create').modal('hide');
				toastr.success('Estudiante registrado con éxito!')
			}).catch(error=>{
				this.errors=error.response.data
			});
		}
	}
})