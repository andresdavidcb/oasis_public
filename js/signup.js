
new Vue({
	el:'#main',
	created:function(){
		//this.getItems();
	},
	data:{
		url:'students/',
		//list:[],
		id_types:[],
		search:'',
		s:{fullname:'',id_type:'',identification:'',birthdate:'',main_phone:'',sec_phone:'',email:'',observations:''},
		c:{name:'',lastname:'',id_type:'',identification:'',birthdate:'',main_phone:'',sec_phone:'',email:'',observations:''},
		e:{id:'',person:'',name:'',lastname:'',id_type:'',identification:'',birthdate:'',main_phone:'',sec_phone:'',email:'',observations:''},
		errors:[]
	},

	methods:{
		
		createItem:function(){
			var url=this.url;
			//alert(url);

			axios.post(url,{
				name:this.c.name,
				lastname:this.c.lastname,
				id_type:this.c.id_type,
				birthdate:this.c.birthdate,
				identification:this.c.identification,
				main_phone:this.c.main_phone,
				sec_phone:this.c.sec_phone,
				email:this.c.email,
				})
			.then(response=>{
				//this.getItems();
				this.errors=[];
				toastr.success('Gracias por preferirnos!')
			}).catch(error=>{
				this.errors=error.response.data
			});
		}
	}
})